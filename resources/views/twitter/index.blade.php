@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Twitter</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                {!! BootForm::open()->action(route('twitter.getFollowers')) !!}
                                {!! BootForm::text('Name', 'name', request()->input('name')) !!}
                                {!! BootForm::submit('Get') !!}
                                {!! BootForm::close() !!}
                            </div>

                            <div class="col-sm-6">
                                <p>

                                </p>
                                <ul>
                                    @foreach(cache('followers_history') as $name)
                                        <li><a href="?name={{ $name }}">{{ $name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-primary">
                    <div class="box-body">

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

