<table class="table table-responsive" id="schedulers-table">
    <thead>
        <th>Account Id</th>
        <th>Account</th>
        <th>Network</th>
        <th>Type</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($schedulers as $scheduler)
        <tr>
            <td>{!! $scheduler->account_id !!}</td>
            <td>{{ $scheduler->account->name }}</td>
            <td>{{ \App\Enum\BaseEnum::getNetworks($scheduler->network) }}</td>
            <td>{!! \App\Enum\BaseEnum::getTypes($scheduler->type) !!}</td>
            <td>{!! $scheduler->status !!}</td>
            <td>
                {!! Form::open(['route' => ['schedulers.destroy', $scheduler->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('schedulers.show', [$scheduler->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('schedulers.edit', [$scheduler->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>