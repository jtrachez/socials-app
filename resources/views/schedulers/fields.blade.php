
<div class="form-group col-sm-12">
    {!! Form::label('account_id', 'Account Id:') !!}
    {!! Form::select('account_id', $accounts, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('network', 'Network:') !!}
    {!! Form::select('network', \App\Enum\BaseEnum::getNetworks(), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::select('type', \App\Enum\BaseEnum::getTypes(), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('status', 'status:') !!}
    {!! Form::checkbox('status') !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('schedulers.index') !!}" class="btn btn-default">Cancel</a>
</div>
