@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Scheculer
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('schedulers.show_fields')
                    <a href="{!! route('schedulers.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
