@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Scheculer
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::open(['route' => 'schedulers.store']) !!}

                        @include('schedulers.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
