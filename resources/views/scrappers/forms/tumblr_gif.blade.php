<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            {!! Form::open(['route' => 'scrapper.gif']) !!}
            <div class="form-group col-sm-8">
                {!! Form::label('url', 'gif page url :') !!}
                {!! Form::text('url', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-8">
                {!! Form::label('start', 'start :') !!}
                {!! Form::text('start', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-8">
                {!! Form::label('end', 'end :') !!}
                {!! Form::text('end', null, ['class' => 'form-control']) !!}
            </div>
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Scrap', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>