<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            {!! Form::open(['route' => 'scrapper.imagefap']) !!}
            <div class="form-group col-sm-8">
                {!! Form::label('url', 'imagefap.com gallery url :') !!}
                {!! Form::text('url', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-8">
                {!! Form::label('type', 'type :') !!}
                {!! Form::select('type', \App\Services\Imagefap::TYPES, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-8">
                {!! Form::label('niche', 'niche :') !!}
                {!! Form::select('niche', \App\Services\Imagefap::NICHES, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Scrap', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>