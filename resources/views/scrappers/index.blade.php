@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Scrappers</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-sm-6">
                @include('scrappers.forms.imagefap_gallery')
            </div>

            <div class="col-sm-6">
                @include('scrappers.forms.tumblr_gif')
            </div>
        </div>
        {{-- Scrapper Image Fap --}}
    </div>
@endsection

