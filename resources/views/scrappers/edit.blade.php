@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Scrapper
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($scrapper, ['route' => ['scrappers.update', $scrapper->id], 'method' => 'patch']) !!}

                        @include('scrappers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection