<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});


Route::get('/test', function () {



    $account = \App\Models\Accounts::where('email', 'claraarabica@gmail.com')->first();
    $account2 = \App\Models\Accounts::where('email', 'cla.raarabica@gmail.com')->first();


//        app(\App\Actions\TwitterActions::class)
//            ->setCredentials($account)
//            ->followAccounts();


    try{
        (new \App\Actions\TwitterActions(new \App\Services\TwitterScrapper(), new \App\Repositories\TwitterRepository()))
            ->setCredentials($account)
            ->followAccounts();
    }catch (\App\Exceptions\BlockedAccountException $e){}
//
//    try{
//        app(\App\Actions\TwitterActions::class)
//            ->setCredentials($account2)
//            ->followAccounts();
//    }catch (\App\Exceptions\BlockedAccountException $e){}




////        ->login('claraarabica@gmail.com','albert2017');
//    $text = app(\App\Services\SpinService::class)->spin(trans('text.post.photo'));
//    dd($text);
    dd();
//    $test = app(\App\Services\SchedulerService::class)->photo();



   dd();

});

Route::get('/rate', function () {
    dd(
        twitter()
            ->setInstance('823966465636306945-7dGIdRbMRxwdtLOkueXZEpwWbyo9piz', 'qE3JyKLpETFnwzTCuOHYOlxrcirW66DevEjEiMPjWtLYW')
            ->rate()
    );
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource('accounts', 'AccountsController');

Route::get('/twitter', 'TwitterController@index')->name('twitter.index');
Route::get('/twitter/callback', 'TwitterController@callback');
Route::get('/twitter/connect', 'TwitterController@connect');
Route::get('/twitter/redirect', 'TwitterController@redirect')->name('twitter.redirect');
Route::post('/twitter/getFollowers', 'TwitterController@getFollowers')->name('twitter.getFollowers');

Route::get('scrapper', 'ScrapperController@index')->name('scrapper.index');
Route::post('scrapper/gif', 'ScrapperController@gif')->name('scrapper.gif');
Route::post('scrapper/imagefap', 'ScrapperController@imagefap')->name('scrapper.imagefap');

Route::resource('schedulers', 'SchedulerController');