<?php

namespace App\Process;

use App\Services\Traits\Output;

abstract class TwitterProcessAbstract
{
    use Output;
}