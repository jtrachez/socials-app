<?php

namespace App\Http\Controllers;

use App\Enum\BaseEnum;
use App\Models\Accounts;
use App\Models\Scheculer;
use App\Repositories\ScheculerRepository;
use App\Services\ValueSelector;
use Illuminate\Http\Request;
use Flash;


class SchedulerController extends AppBaseController
{
    /** @var  ScheculerRepository */
    private $schedulerRepository;
    /**
     * @var ValueSelector
     */
    private $valueSelector;

    public function __construct(ScheculerRepository $schedulerRepo, ValueSelector $valueSelector)
    {
        $this->schedulerRepository = $schedulerRepo;
        $this->valueSelector = $valueSelector;
    }

    public function index()
    {
        $schedulers = $this->schedulerRepository->all();


//        if($type = request('type')){
//            $schedulers->findWhere(['type' => $type]);
//        }


        return view('schedulers.index')
            ->with('schedulers', $schedulers);
    }

    /**
     * Show the form for creating a new Scheculer.
     *
     * @return Response
     */
    public function create()
    {
        $accounts = Accounts::all();

        return view('schedulers.create')
            ->withAccounts($accounts->pluck('name','id'));
    }

    public function store(Request $request)
    {
        $input = $request->all();

        Scheculer::create([
            'account_id' => $input['account_id'],
            'network' =>$input['network'],
            'type' => $input['type'],
            'status' => 1
        ]);

        Flash::success('Scheculer saved successfully.');

        return redirect(route('schedulers.index'));
    }


    public function show($id)
    {
        $scheduler = $this->schedulerRepository->findWithoutFail($id);

        if (empty($scheduler)) {
            Flash::error('Scheculer not found');

            return redirect(route('schedulers.index'));
        }

        return view('schedulers.show')->with('scheduler', $scheduler);
    }

    public function edit($id)
    {
        $accounts = Accounts::all();

        $scheduler = $this->schedulerRepository->findWithoutFail($id);

        if (empty($scheduler)) {
            Flash::error('Scheculer not found');

            return redirect(route('schedulers.index'));
        }

        return view('schedulers.edit')->with('scheduler', $scheduler)->withAccounts($accounts->pluck('name','id'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $scheduler = $this->schedulerRepository->findWithoutFail($id);

        $status = !isset($input['status']) ? false : true;
        if (empty($scheduler)) {
            Flash::error('Scheculer not found');

            return redirect(route('schedulers.index'));
        }

        $scheduler = $this->schedulerRepository->update([
            'account_id' => $input['account_id'],
            'network' => $input['network'],
            'type' =>  $input['type'],
            'status' => $status
        ], $id);

        Flash::success('Scheculer updated successfully.');

        return redirect(route('schedulers.index'));
    }


    public function destroy($id)
    {
        $scheduler = $this->schedulerRepository->findWithoutFail($id);

        if (empty($scheduler)) {
            Flash::error('Scheculer not found');

            return redirect(route('schedulers.index'));
        }

        $this->schedulerRepository->delete($id);

        Flash::success('Scheculer deleted successfully.');

        return redirect(route('schedulers.index'));
    }
}
