<?php

namespace App\Http\Controllers;

use App\Models\Accounts;

class TwitterController extends Controller
{

    public function index()
    {
        $accounts = Accounts::all();

        return view('twitter.index')
            ->withAccounts($accounts);
    }

    public function getFollowers()
    {
        $account = Accounts::inRandomOrder()->take(1)->first();

        twitter()->setInstance($account->twitter_token, $account->twitter_token_secret)
            ->getAllFollowersByName(request()->input('name'));

        return redirect(route('twitter.index'));

    }

    public function callback()
    {
        $user = \Socialite::driver('twitter')->user();

        Accounts::create([
            'name' => $user->nickname,
            'twitter_id' => $user->id,
            'twitter_token' => $user->token,
            'twitter_token_secret' => $user->tokenSecret,
            'twitter_followers' => $user->user['followers_count'],
        ]);

        return redirect(route('accounts.index'));
    }
    public function connect()
    {
        return '<a href="'.route('twitter.redirect').'">Connect</a>';
    }
     public function redirect()
    {
        return \Socialite::driver('twitter')->redirect();
    }




}
