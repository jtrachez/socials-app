<?php

namespace App\Http\Controllers;

use App\DataTables\AccountsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAccountsRequest;
use App\Http\Requests\UpdateAccountsRequest;
use App\Repositories\AccountsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AccountsController extends AppBaseController
{
    /** @var  AccountsRepository */
    private $accountsRepository;

    public function __construct(AccountsRepository $accountsRepo)
    {
        $this->accountsRepository = $accountsRepo;
    }

    /**
     * Display a listing of the Accounts.
     *
     * @param AccountsDataTable $accountsDataTable
     * @return Response
     */
    public function index(AccountsDataTable $accountsDataTable)
    {
        return $accountsDataTable->render('accounts.index');
    }

    /**
     * Show the form for creating a new Accounts.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounts.create');
    }

    /**
     * Store a newly created Accounts in storage.
     *
     * @param CreateAccountsRequest $request
     *
     * @return Response
     */
    public function store(CreateAccountsRequest $request)
    {
        $input = $request->all();

        $accounts = $this->accountsRepository->create($input);

        Flash::success('Accounts saved successfully.');

        return redirect(route('accounts.index'));
    }

    /**
     * Display the specified Accounts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $accounts = $this->accountsRepository->findWithoutFail($id);

        if (empty($accounts)) {
            Flash::error('Accounts not found');

            return redirect(route('accounts.index'));
        }

        return view('accounts.show')->with('accounts', $accounts);
    }

    /**
     * Show the form for editing the specified Accounts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $accounts = $this->accountsRepository->findWithoutFail($id);

        if (empty($accounts)) {
            Flash::error('Accounts not found');

            return redirect(route('accounts.index'));
        }

        return view('accounts.edit')->with('accounts', $accounts);
    }

    /**
     * Update the specified Accounts in storage.
     *
     * @param  int              $id
     * @param UpdateAccountsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccountsRequest $request)
    {
        $accounts = $this->accountsRepository->findWithoutFail($id);

        if (empty($accounts)) {
            Flash::error('Accounts not found');

            return redirect(route('accounts.index'));
        }

        $accounts = $this->accountsRepository->update($request->all(), $id);

        Flash::success('Accounts updated successfully.');

        return redirect(route('accounts.index'));
    }

    /**
     * Remove the specified Accounts from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $accounts = $this->accountsRepository->findWithoutFail($id);

        if (empty($accounts)) {
            Flash::error('Accounts not found');

            return redirect(route('accounts.index'));
        }

        $this->accountsRepository->delete($id);

        Flash::success('Accounts deleted successfully.');

        return redirect(route('accounts.index'));
    }
}
