<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateScrapperRequest;
use App\Http\Requests\UpdateScrapperRequest;
use App\Repositories\ScrapperRepository;
use App\Http\Controllers\AppBaseController;
use App\Services\GifService;
use App\Services\Imagefap;
use Goutte\Client;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ScrapperController extends AppBaseController
{

    /**
     * @var Imagefap
     */
    private $imagefap;
    /**
     * @var GifService
     */
    private $gif;

    public function __construct(Imagefap $imagefap, GifService $gif)
    {
        $this->imagefap = $imagefap;
        $this->gif = $gif;
    }

    /**
     * Display a listing of the Scrapper.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('scrappers.index');
    }

    public function imagefap(Request $request)
    {
        $url = $request->input('url');
        $type = $request->input('type');
        $niche = $request->input('niche');

        $path = $this->imagefap->scrapForGallery($url, $type, $niche)
            ->save();

        Flash::success("Images is scrapped on <strong>$path</strong>");

        return redirect(route('scrapper.index'));

    }

    public function gif()
    {
        $start = request()->input('start');
        $end = request()->input('end');
        $url = request()->input('url');

        $this->gif->scrapAllPagesGifs($url, $start, $end)->save();

        return redirect(route('scrapper.index'));
    }


}
