<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 07/02/2017
 * Time: 18:55
 */

namespace App\Services;


use App\Services\Traits\Helper;
use App\Services\Traits\Images;
use Goutte\Client;

class GifService
{
    use Helper, Images;

    protected $gifs = [];

    public function scrapPageGifs($url, $iterator)
    {
        $crawler = $this->client()->request('GET', $url . 'page/' . $iterator);
        $crawler->filter('img')->each(function ($node) {
            $src = $node->attr('src');
            if (ends_with($src, 'gif')) {
                $this->gifs[] = $src;
                return $src;
            }
            return $src;
        });
    }

    public function scrapAllPagesGifs($url, $start, $end)
    {
        foreach (range($start, $end) as $iterator) {
            $this->scrapPageGifs($url, $iterator);
        }

        return $this;
    }

    public function save()
    {
        return $this->saveImages('gif', $this->gifs);
    }

    protected function getDistinationFolder($service)
    {
        return public_path($service);
    }

    private function client()
    {
        return new Client();
    }
}