<?php
namespace App\Services;

use App\Services\Traits\Helper;
use App\Services\Traits\Images;
use Goutte\Client;

class Imagefap
{
    use Helper, Images;
    const TYPES = ['gallery', 'post'];
    const NICHES = ['black', 'tatoo', 'onegirl', 'beauty', 'ass', 'bikini', 'gallery', 'asian'];
    const FILTER_GALELRY = 'td > a';
    const FILTER_IMAGE = 'span';

    public $images = [];
    protected $niche;
    protected $type;

    public function scrapForGallery($url, $type, $niche)
    {
        $this->setDistinationFolder($type, $niche);

        if (cache("images_$url")) {
            $this->images = cache("images_$url");
            return $this;
        }

        $crawler = $this->client()->request('GET', $url);

        $links = $crawler->filter(static::FILTER_GALELRY)
            ->each(function ($node) {
                $href = $node->attr('href');
                return
                    starts_with($href, '/photo/')
                        ? $this->scrapImageFap($href)
                        : null;
            });


        $this->images = $this->filterArray($links);

        cache(["images_$url" => $this->images], 60);

        return $this;
    }

    private function setDistinationFolder($type, $niche)
    {
        $this->type = $type;
        $this->niche = $niche;
    }


    protected function getDistinationFolder($service)
    {
        $path =
            static::TYPES[$this->type] . '/' .
            static::NICHES[$this->niche] . '/' .
            $service;

        if(static::TYPES[$this->type] != 'post'){
            $path .= uniqid('_');
        }

        return public_path($path);
    }

    private function scrapImageFap($href)
    {
        $crawler = $this->client()->request('GET', 'http://www.imagefap.com' . $href);
        $pic = $crawler->filter(static::FILTER_IMAGE)->each(function ($node) {
            if (starts_with($node->html(), 'http://x.imagefapusercontent.com/u/')) {
                return $node->html();
            }
            return null;
        });

        return current($this->filterArray($pic));

    }

    public function save()
    {
        return $this->saveImages('imagefap', $this->images);
    }

    private function client()
    {
        return new Client();
    }
}