<?php
namespace App\Services;


use App\Enum\BaseEnum;
use Illuminate\Support\Facades\File;

class ValueSelector
{
    public function getGif()
    {
        $path = public_path('gif');
        return collect(glob($path . '/*.gif'))->random();
    }

    public function getPhoto()
    {
        $files = File::allFiles(public_path('post'));

        $files = collect($files)->map(function ($file) {
            return $file->getRealPath();
        });

        return $files->random();
    }

    public function getTweetTimeline($timeline)
    {
        $tweet = collect($timeline)->random();
        return $tweet->id;
    }

    public function getQueryForFamousTwitterAccounts()
    {
       return 'from:' . implode(BaseEnum::TWITTER_FAMOUS_ACCOUNT, '+OR+from:');
    }
}