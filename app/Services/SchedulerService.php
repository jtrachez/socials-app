<?php
namespace App\Services;


use App\Actions\TwitterActions;
use App\Exceptions\BlockedAccountException;
use App\Models\Scheculer;
use App\Repositories\TwitterRepository;
use App\Services\Traits\Output;


class SchedulerService
{

    use Output;


    private $valueSelector;
    /**
     * @var TwitterRepository
     */
    private $twitterRepository;
    /**
     * @var TwitterActions
     */
    private $twitterActions;

    /**
     * SchedulerService constructor.
     * @param ValueSelector $valueSelector
     * @param TwitterRepository $twitterRepository
     * @param TwitterActions $twitterActions
     */
    public function __construct(ValueSelector $valueSelector, TwitterRepository $twitterRepository, TwitterActions $twitterActions)
    {
        $this->valueSelector = $valueSelector;
        $this->twitterRepository = $twitterRepository;
        $this->twitterActions = $twitterActions;
    }

    public function massFollow()
    {
        foreach ($this->getScheduleAccounts('follow') as $schedule) {
            $this->line('> Start follow account : ' . $schedule->account->name);
            try {
                $this->twitterActions
                    ->setCredentials($schedule->account)
                    ->followAccounts()
                    ->countFollowers();
            } catch (BlockedAccountException $e) {
                $this->line('/!\ Account blocked : ' . $e->getMessage());
            }
        }
    }

    public function countFollowers()
    {
//        dd($this->getScheduleAccounts('count_followers')->toArray());
        $this->getScheduleAccounts('count_followers')
            ->each(function ($schedule) {
                $this->twitterActions
                    ->setCredentials($schedule->account)
                    ->countFollowers();
            });
    }

    public function gif()
    {
        foreach ($this->getScheduleAccounts('gif') as $schedule) {
            twitter()
                ->setInstance($schedule->account->twitter_token, $schedule->account->twitter_token_secret)
                ->postGif($this->valueSelector->getGif());

            $this->output->writeln('GIF added on ' . $schedule->account->name);
        }
    }

    public function photo()
    {
        foreach ($this->getScheduleAccounts('photo') as $schedule) {
            twitter()
                ->setInstance($schedule->account->twitter_token, $schedule->account->twitter_token_secret)
                ->postPhoto($this->valueSelector->getPhoto());
            $this->output->writeln('PHOTO added on ' . $schedule->account->name);

        }
    }

    public function text()
    {
        foreach ($this->getScheduleAccounts('tweet') as $schedule) {
            twitter()
                ->setInstance($schedule->account->twitter_token, $schedule->account->twitter_token_secret)
                ->postText($this->valueSelector->getText());
        }
    }

    public function retweetTimeline()
    {
        foreach ($this->getScheduleAccounts('retweet_timeline') as $schedule) {
            $timeline = twitter()
                ->setInstance($schedule->account->twitter_token, $schedule->account->twitter_token_secret)
                ->getUserTimeline();

            $tweet_id = $this->valueSelector->getTweetTimeline($timeline);
            twitter()->retweet($tweet_id);
            $this->output->writeln('Retweet done with ' . $schedule->account->name);

        }
    }

    public function retweetFamous()
    {
        foreach ($this->getScheduleAccounts('retweet_famous') as $schedule) {
            $tweets = twitter()
                ->setInstance($schedule->account->twitter_token, $schedule->account->twitter_token_secret)
                ->getFamousTweets($this->valueSelector->getQueryForFamousTwitterAccounts());
            dd(collect($tweets->statuses)->filter(function ($item) {
//                dump($item->retweeted_status);
                if (!isset($item->retweeted_status)) {
                    return $item;
                };
            })->random());
            twitter()->retweet(collect($tweets->statuses)->random()->id);
            $this->output->writeln('Retweet famous done with ' . $schedule->account->name);

        }
    }

    private function getScheduleAccounts($type)
    {
        return Scheculer::where(['type' => $type, 'status' => 1])
            ->with('account')
            ->get();
    }
}