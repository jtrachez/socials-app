<?php

namespace App\Services;


use App\Enum\TwitterEnum;
use App\Exceptions\BlockedAccountException;
use App\Models\Accounts;
use App\Services\Traits\BannedAccount;
use App\Services\Traits\CookiesManager;
use Goutte\Client;
use Illuminate\Support\Facades\Log;

class TwitterScrapper
{

    use CookiesManager, BannedAccount;

    protected $cookies;
    protected $account;
    /**
     * @var $client Client
     */
    private $client;


    /**
     * @param $account
     * @return $this
     */
    public function init($account)
    {

        $this->account = $account;

        $this->cookies = $this->getCacheCookie($account->email) ?? null;
        $this->client = $this->setClient($this->cookies);

        return $this;
    }

    private function setClient($cookie)
    {

        $client = new Client(
            [
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; U; Android 4.0; xx-xx; GT-I9305 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
            ],
            null,
            $cookie);
//        $client->setClient(new \GuzzleHttp\Client(
//            ['proxy' => 'http://181.41.210.240:80']
//        ));
        return $client;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function login()
    {
        $request = $this->client
            ->request('GET', TwitterEnum::LOGIN_URL);

        $this->checkIfBannedAccount($request);

        if ($this->cookies) {
            return $this;
        }

        $form = $request->selectButton(TwitterEnum::LOGIN_BUTTON)->form();

        $form['session[username_or_email]'] = $this->account->email;
        $form['session[password]'] = $this->account->password;

        $response = $this->client->submit($form);

        $cookies = $this->client->getCookieJar();
        $this->setCacheCookie($this->account->email, $cookies);

        return $this;

    }

    public function postTweet()
    {
        $request = $this->client
            ->request('GET', TwitterEnum::TWEET_URL);
        $this->checkIfBannedAccount($request);

        $form = $request->selectButton(TwitterEnum::TWEET_BUTTON)->form();

        $form['tweet[text]'] = 'Bonsoir <3';

        $this->client->submit($form);

        return $this;

    }

    public function sleep($seconds)
    {
        sleep($seconds);

        return $this;
    }

    public function follow($username)
    {
        $request = $this->client
            ->request('GET', TwitterEnum::BASE_URL . '/' . $username);

        $this->checkIfBannedAccount($request);

        $followButton = $request->selectButton(TwitterEnum::FOLLOW_BUTTON);

        if (!$followButton->getNode(0)) {
            Log::notice('Follow button not found for ' . $this->account->email);
            return $this;
        }

        $response = $this->client->submit($followButton->form());

        if ($response->filter('.w-button-default')) {
            Log::info($this->account->email . ' follow ' . $username);
        }

        return $this;
    }

    public function countFollowers()
    {
        $request = $this->client
            ->request('GET', TwitterEnum::PROFILE_URL);

        $this->checkIfBannedAccount($request);

        $node_followers = $request->filter('.statnum')->getNode(2);
        $node_followings = $request->filter('.statnum')->getNode(1);

        if ($node_followers && $node_followings) {
            $account = Accounts::where('email', $this->account->email)->first()->update([
                'twitter_followers' => $node_followers->nodeValue,
                'twitter_followings' => $node_followings->nodeValue
            ]);
        }

        return $this;
    }


    protected function debug($request)
    {
        echo $request;
        die;
    }

}