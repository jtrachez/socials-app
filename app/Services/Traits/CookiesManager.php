<?php

namespace App\Services\Traits;


trait CookiesManager
{
    public function setCacheCookie($email, $cookies)
    {
        cache()->forever($this->cacheKey() . $email, $cookies);
    }

    public function getCacheCookie($email)
    {
        return cache($this->cacheKey() . $email);
    }

    function cacheKey(){
        return 'cookies_for_';
    }
}