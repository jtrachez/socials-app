<?php

namespace App\Services\Traits;


trait Following
{
    function saveFollowings($followings)
    {
        $this->filterAlreadyFollowing($followings)
        ->each(function ($user) {
            \App\Models\Following::firstOrCreate([
                'twitter_id' => $user->id,
                'name' => $user->screen_name,
                'lang' => $user->lang,
            ]);
        });
    }
}