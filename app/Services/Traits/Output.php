<?php

namespace App\Services\Traits;

use Symfony\Component\Console\Output\OutputInterface;

trait Output
{
    /**
     * Output interface.
     *
     * @var OutputInterface
     */
    protected $output;

    /**
     * Set output interface.
     *
     * @param OutputInterface $output
     * @return mixed
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Write a string as information output.
     *
     * @param  string $string
     */
    public function info($string)
    {
        if (!$this->output) {
            return;
        }

        $this->output->writeln("<info>$string</info>");
    }

    /**
     * Write a string as standard output.
     *
     * @param  string $string
     */
    public function line($string)
    {
        if (!$this->output) {
            return;
        }

        $this->output->writeln($string);
    }

    /**
     * Write a string as comment output.
     *
     * @param  string $string
     */
    public function comment($string)
    {
        if (!$this->output) {
            return;
        }

        $this->output->writeln("<comment>$string</comment>");
    }

    /**
     * Write a string as error output.
     *
     * @param  string $string
     */
    public function error($string)
    {
        if (!$this->output) {
            return;
        }

        $this->output->writeln("<error>$string</error>");
    }

    /**
     * Write a string prefixed by a check mark.
     *
     * @param      $string
     */
    public function done($string)
    {
        if (!$this->output) {
            return;
        }

        $this->output->writeln("<info>✔</info> $string");
    }
}
