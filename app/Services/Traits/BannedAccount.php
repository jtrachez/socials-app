<?php

namespace App\Services\Traits;

use App\Enum\BannedEnum;
use App\Exceptions\BlockedAccountException;
use App\Models\Accounts;

trait BannedAccount
{

    protected function checkIfBannedAccount($response)
    {
        $node = $response->filter('.PageHeader');

        if ($node->getNode(0)) {
            $reason = $this->getTypeOfBan($node->html());
            throw new BlockedAccountException('This account is blocked : ' . $this->account->name . ' reason : ' . $reason, $this->account);
        }
    }

    protected function getTypeOfBan($message)
    {
        return trim($message) === BannedEnum::CHANGE_PASSWORD ?
            trim($message) : 'Unknow reason';
    }

    protected function setAccountStatus($status)
    {
        Accounts::where('email', $this->account->email)->update([
            'status' => $status
        ]);
    }
}