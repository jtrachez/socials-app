<?php

namespace App\Services\Traits;


trait Dm
{
    public function getDm()
    {
        $options = [
            'count' => '100'
        ];
        return $this->service->getDmsIn($options);
    }
}