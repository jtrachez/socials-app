<?php


namespace App\Services\Traits;


trait Spin
{

    public function spin($text)
    {
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
            array($this, 'replace'),
            $text
        );
    }
    public function replace($text)
    {
        $text = $this->spin($text[1]);
        $parts = explode('|', $text);
        return $parts[array_rand($parts)];
    }

}