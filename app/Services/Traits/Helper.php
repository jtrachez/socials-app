<?php
namespace App\Services\Traits;

use Illuminate\Support\Collection;

trait Helper
{
    function filterAlreadyFollowing($data)
    {
        if (is_object($data) && !is_a(Collection::class, get_class($data), true)) {
            $data = collect($data);
        }

        return $data->filter(function ($value) {
            return $value->following != true;
        });
    }

    function filterArray($array)
    {
        return
            array_values(
                array_filter(
                    $array
                )
            );
    }

    function setFollowersHistory($name)
    {
        $followersHistory = cache('followers_history');
        $followersHistory[] = $name;
        cache()->forever('followers_history', array_unique($followersHistory));
    }
}