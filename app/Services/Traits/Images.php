<?php
namespace App\Services\Traits;

trait Images
{
    function saveImages($service, $images)
    {
        $path = $this->getDistinationFolder($service);
        if (!mkdir($path, 0777, true)) {
            throw new \Exception('Impossible to create folder ' . $path);
        }

        collect($images)->each(function ($image) use ($path, $images) {
            $file = $path . '/' . uniqid() . basename($image);
            file_put_contents($file, file_get_contents($image));
        });

        return $path;
    }
}