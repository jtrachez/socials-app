<?php
namespace App\Services;

use App\Services\Traits\Dm;
use App\Services\Traits\Following;
use App\Services\Traits\Helper;
use App\Services\Traits\Spin;
use Thujohn\Twitter\Twitter;

class TwitterService
{

    use Helper, Spin, Following, Dm;

    const FOLLOWERS_COUNT = 200;
    const FOLLOWERS_GET = 20;
    const RATE_LIMIT = 1;

    protected $service;


    public function __construct(Twitter $service)
    {
        $this->service = $service;
    }

    public function setInstance($token, $secret)
    {
        $opts = [
            'token' => $token,
            'secret' => $secret,
        ];

        $this->service->reconfig($opts);

        return $this;
    }

    public function getFollowersByName($name, $cursor = false)
    {
        $opts = [
            'screen_name' => $name,
            'count' => static::FOLLOWERS_COUNT,
        ];

        if ($cursor) {
            $opts['cursor'] = $cursor;
        }

        return $this->service->getFollowers($opts);
    }

    public function getAllFollowersByName($name)
    {
        $data = collect([]);
        $i = 0;
        do {
            $followers = ($i == 0) ?
                $this->getFollowersByName($name) :
                $this->getFollowersByName($name, $followers->next_cursor);

            $data = $data->merge($followers->users);
            cache()->forever('tw_data', $data);
            $this->saveFollowings($data);
            $i++;
        } while ($i < static::RATE_LIMIT);

        $this->setFollowersHistory($name);
        return $data;
    }

    public function postGif($gif)
    {
        $this->postMedia($gif, $this->spin(trans('text.post.gif')));
    }

    public function postPhoto($photo)
    {
        $this->postMedia($photo, $this->spin(trans('text.post.photo')));
    }

    public function postText($text = '')
    {
        $text = empty($text) ? $this->spin(trans('text.post.text')) : $text;
        $this->service->postTweet(['status' => $text]);
    }

    public function followUser($id)
    {
        return $this->service->postFollow(['user_id' => $id]);
    }

    public function postRetweetFromFamousAccount()
    {

    }

    public function getCredentials()
    {
        return $this->service->getCredentials();
    }

    public function rate()
    {
        return $this->service->getAppRateLimit();
    }

    public function getUserTimeline()
    {
        return $this->service->getHomeTimeline();
    }

    public function retweet($id)
    {
        return $this->service->postRt($id);
    }

    public function getFamousTweets($accounts)
    {
        $options = [
            'q' => urlencode($accounts),
            'count' => 20,
            'include_entities' => 0
        ];

        return $this->service->getSearch($options);
    }

    public function postMedia($media, $text)
    {
        $media = $this->service->uploadMedia(['media' => file_get_contents($media)]);
        $this->service->postTweet([
            'status' => $text,
            'media_ids' => $media->media_id
        ]);
    }

    public function getClient()
    {
        return $this->service;
    }

}