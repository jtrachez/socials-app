<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $fillable = [
      'twitter_id',
      'name',
      'lang',
      'network',
    ];
}
