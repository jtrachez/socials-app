<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Accounts
 * @package App\Models
 * @version January 23, 2017, 8:53 pm UTC
 */
class Accounts extends Model
{

    public $table = 'accounts';


    public $fillable = [
        'name',
        'email',
        'password',
        'twitter_id',
        'twitter_token',
        'twitter_token_secret',
        'twitter_followers',
        'twitter_followings',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function schedulers()
    {
        return $this->hasMany(Scheculer::class);
    }
}
