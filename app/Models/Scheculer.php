<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scheculer extends Model
{

    public $table = 'schedulers';

    public $fillable = [
        'account_id',
        'network',
        'type',
        'status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'account_id' => 'integer'
    ];


    public function account()
    {
        return $this->belongsTo(Accounts::class);
    }
    
}
