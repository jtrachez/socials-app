<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Followers extends Model
{

    protected $fillable = [
        'account_id',
        'twitter_id',
        'network'
    ];
}
