<?php
namespace App\Enum;

class StatusEnum
{
    const ACTIVE = 1;
    const DEACTIVE = 0;
}