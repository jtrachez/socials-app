<?php
namespace App\Enum;

class BaseEnum
{
    const NETWORKS_LIST = ['twitter', 'facebook', 'instagram'];
    const TYPES_LIST = ['gif', 'news', 'video', 'rt', 'tweet',
        'follow','retweet_famous', 'retweet_timeline', 'photo',
        'count_followers'];
    const NB_FOLLOW = 20;
    const TWITTER_FAMOUS_ACCOUNT = [
        'PSG_inside',
        'OM_Officiel',
//        'CamilleCombal',
//        'Nabilla',
//        'AmazedByAnimals',
//        'Qofficiel',
//        'LeVraiHoroscope',
//        'viedemerde',
//        'Paris',
//        'BabyAnimalGifs',
//        'M6',
    ];


    static function getNetworks($key = false)
    {
        if($key){
            return static::combine(static::NETWORKS_LIST)[$key];
        }
        return static::combine(static::NETWORKS_LIST);
    }

    static function getTypes($key = false)
    {
        if($key){
            return static::combine(static::TYPES_LIST)[$key];
        }
        return static::combine(static::TYPES_LIST);
    }

    static function combine($array)
    {
        return array_combine($array, $array);
    }
}