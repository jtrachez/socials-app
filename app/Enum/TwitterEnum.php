<?php

namespace App\Enum;

class TwitterEnum
{
    const BASE_URL = 'https://mobile.twitter.com';

    const LOGIN_URL = 'https://mobile.twitter.com/session/new';

    const LOGOUT_URL = 'https://mobile.twitter.com/session/new';
    const TWEET_URL = 'https://mobile.twitter.com/compose/tweet';
    const PROFILE_URL = 'https://mobile.twitter.com/account';

    const LOGIN_BUTTON = 'Log in';
    const FOLLOW_BUTTON = 'Suivre';
    const TWEET_BUTTON = 'Tweeter';


}