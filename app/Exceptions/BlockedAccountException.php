<?php

namespace App\Exceptions;

use App\Services\Traits\BannedAccount;
use Exception;
use Illuminate\Support\Facades\Log;

class BlockedAccountException extends Exception
{
    use BannedAccount;
    protected $account;

    public function __construct($message = '', $account)
    {
        parent::__construct($message);
        $this->account = $account;
        $this->setAccountStatus(0);
        Log::warning($message);
    }
}