<?php

namespace App\Actions;

use App\Models\Accounts;
use App\Repositories\TwitterRepository;
use App\Services\Traits\Output;
use App\Services\TwitterScrapper;
use Illuminate\Contracts\Logging\Log;

class TwitterActions
{
    use Output;

    protected $account;

    /**
     * @var TwitterScrapper
     */
    private $scrapper;
    /**
     * @var TwitterRepository
     */
    private $twitterRepository;

    public function __construct(
        TwitterScrapper $scrapper,
        TwitterRepository $twitterRepository)
    {
        $this->scrapper = $scrapper;
        $this->twitterRepository = $twitterRepository;
    }

    public function setCredentials($account)
    {
        $this->account = $account;

        $this->scrapper
            ->init($this->account);

        return $this;
    }

    public function postTweet()
    {
        $this->scrapper
            ->login()
            ->postTweet();
    }

    public function followAccounts()
    {
        $scrapper = $this->scrapper
            ->login();

        $followings = $this->twitterRepository->getRandomFollowers($this->account->id);

        $followings->each(function ($following) use ($scrapper) {
            $scrapper
                ->follow($following->name)
                ->sleep(rand(1, 6));
        });
        return $this;
    }

    public function countFollowers()
    {
        $this->scrapper
            ->login()
            ->countFollowers();

        return $this;
    }

}