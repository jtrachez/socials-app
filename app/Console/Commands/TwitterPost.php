<?php

namespace App\Console\Commands;

use App\Services\SchedulerService;
use Illuminate\Console\Command;

class TwitterPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:post {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post on twitter';
    /**
     * @var SchedulerService
     */
    private $scheduler;

    /**
     * Create a new command instance.
     *
     * @param SchedulerService $scheduler
     * @internal param TwitterService $twitter
     */
    public function __construct(SchedulerService $scheduler)
    {
        parent::__construct();
        $this->scheduler = $scheduler;
    }

    public function handle()
    {
        $this->scheduler->setOutput($this->output);

        if($this->argument('type') == 'gif'){
            $this->scheduler->gif();
        }

        if($this->argument('type') == 'follow'){
            $this->scheduler->massFollow();
        }

        if($this->argument('type') == 'photo'){
            $this->scheduler->photo();
        }

        if($this->argument('type') == 'tweet'){
            $this->scheduler->text();
        }

        if($this->argument('type') == 'retweet_timeline'){
            $this->scheduler->retweetTimeline();
        }

        if($this->argument('type') == 'count_followers'){
            $this->scheduler->countFollowers();
        }

        if($this->argument('type') == 'retweet_famous'){
//            $this->scheduler->retweetFamous();
        }

    }
}
