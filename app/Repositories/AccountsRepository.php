<?php

namespace App\Repositories;

use App\Models\Accounts;
use InfyOm\Generator\Common\BaseRepository;

class AccountsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Accounts::class;
    }
}
