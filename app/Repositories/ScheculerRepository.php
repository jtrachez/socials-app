<?php

namespace App\Repositories;

use App\Models\Scheculer;
use InfyOm\Generator\Common\BaseRepository;

class ScheculerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'account_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Scheculer::class;
    }
}
