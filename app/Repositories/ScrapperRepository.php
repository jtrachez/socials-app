<?php

namespace App\Repositories;

use App\Models\Scrapper;
use InfyOm\Generator\Common\BaseRepository;

class ScrapperRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'url'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Scrapper::class;
    }
}
