<?php
namespace App\Repositories;


use App\Enum\BaseEnum;
use App\Models\Followers;
use App\Models\Following;
use Illuminate\Database\Eloquent\Collection;

class TwitterRepository
{

    /**
     * @param $account_id
     * @return Collection
     */
    public function getRandomFollowers($account_id)
    {
        return Following::inRandomOrder()
            ->where('lang', 'fr')
            ->whereNotIn('twitter_id', $this->alreadyFollowing($account_id))
            ->take(BaseEnum::NB_FOLLOW)->get();
    }

    public function alreadyFollowing($account_id)
    {
        return Followers::where('account_id', $account_id)
            ->get()->pluck('twitter_id')->toArray();
    }


}